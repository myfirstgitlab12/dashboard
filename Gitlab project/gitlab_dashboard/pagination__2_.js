//Navbar Code (About/More Info/Contact)
var prevScrollpos = window.pageYOffset;
window.onscroll = function() {
var currentScrollPos = window.pageYOffset;
  if (prevScrollpos > currentScrollPos) {
    document.getElementById("navbar").style.top = "0";
  } else {
    document.getElementById("navbar").style.top = "-50px";
  }
  prevScrollpos = currentScrollPos;
}

// Down Excel File

function exportToExcel(){
    var downloadurl;
    var dataFileType = 'application/vnd.ms-excel';
    var tableSelect = document.getElementsByTagName("table")[0];
    var tableHTMLData = tableSelect.outerHTML.replace(/ /g, '%20');
      
    filename = 'issues.xls';
        
    downloadurl = document.createElement("a");
    document.body.appendChild(downloadurl);
        
    if(navigator.msSaveOrOpenBlob){
    var blob = new Blob(['ufeff', tableHTMLData], {
        type: dataFileType
    });
    navigator.msSaveOrOpenBlob(blob, filename);
    }else{
    downloadurl.href =  'data:' + dataFileType + ', ' + tableHTMLData;
    downloadurl.download = filename;
        
    downloadurl.click();
    }
}

// Dropdown Filter
 function getUniqueValuesFromColoum(){
	var unique_col_value = {}
	
		allfilter = document.querySelectorAll(".table-filter")
		allfilter.forEach((filter_i)=>{
			col_index = filter_i.parentElement.getAttribute("col-index");
			const rows = document.querySelectorAll("#tbl > tbody > tr")
			rows.forEach((row)=>{
				cell_value = row.querySelector("td:nth-child("+col_index+")").innerHTML;
				if(col_index in unique_col_value){
					if (unique_col_value[col_index].includes(cell_value)){
					}else{
						unique_col_value[col_index].push(cell_value)
					}
									
				}else{
					unique_col_value[col_index] = new Array(cell_value)
				}
			});
		});
		updateSelectOptions(unique_col_value)
			
}
// Add <option> tags

function updateSelectOptions(unique_col_value){
	allfilter = document.querySelectorAll(".table-filter")
	allfilter.forEach((filter_i) => {
			col_index = filter_i.parentElement.getAttribute("col-index")
			unique_col_value[col_index].forEach((i) => {
			  filter_i.innerHTML = filter_i.innerHTML + `\n<option value="${i}">${i}</option>` 
			});
	});
}
// create filter row functions

function filter_rows(){
	allfilter = document.querySelectorAll(".table-filter")
	var filter_value = {}
	
	allfilter.forEach((filter_i)=>{
		col_index = filter_i.parentElement.getAttribute("col-index")
		
		value = filter_i.value
		if(value != "all"){
			filter_value[col_index] = value;		
		}

	});
	
	var col_cell_value = {};

	const rows = document.querySelectorAll("#tbl > tbody > tr");
	rows.forEach((row)=>{
		var display_row = true;

		allfilter.forEach((filter_i)=>{
			col_index = filter_i.parentElement.getAttribute("col-index")
			col_cell_value[col_index] = row.querySelector("td:nth-child("+ col_index + ")").innerHTML;
		})
		for (var col_i in filter_value){
			filter_val = filter_value[col_i]
			row_cell_value = col_cell_value[col_i]
			
			if(row_cell_value.indexOf(filter_val) == -1 && filter_val != "all" ){
				display_row = false;
				break;
			}
		}
		if(display_row == true){
			row.style.display = "table-row"
		}else {
			row.style.display = "none"
		}
	})
}
window.onload = () => {
	//document.querySelector("#tbl > tbody > tr:nth-child(1) > td:nth-child(2)").innerHTML;
  getUniqueValuesFromColoum();
}


// Pagination Code
var a = document.getElementById("#tbl");
var inputVal = "";
if (a) {
    inputVal = a.value;
}
// number of rows per page
// number of rows of the table
var rows = a.rows.length;
// get the first cell's tag name (in the first row)
 var firstRow = a.rows[0].firstElementChild.tagName;
// boolean var to check if table has a head row
var hasHead = (firstRow === "TH");
// an array to hold each row
var tr = [];
var n = 20;
// loop counters, to start count from rows[1] (2nd row) if the first row has a head tag
var i, ii, j = (hasHead)?1:0;
// holds the first row if it has a (<TH>) & nothing if (<TD>)
var th = (hasHead?a.rows[(0)].outerHTML:"");
// count the number of pages
var pageCount = Math.ceil(rows / n);
// if we had one page only, then we have nothing to do ..
if (pageCount > 1) {
	// assign each row outHTML (tag name & innerHTML) to the array
	for (i = j,ii = 0; i < rows; i++, ii++)
		tr[ii] = a.rows[i].outerHTML;
	// create a div block to hold the buttons
	a.insertAdjacentHTML("afterend","<div id='buttons'></div");
	// the first sort, default page is the first one
	sort(1);
}

// ($p) is the selected page number. it will be generated when a user clicks a button
function sort(p) {
	/* create ($rows) a variable to hold the group of rows
	** to be displayed on the selected page,
	** ($s) the start point .. the first row in each page, Do The Math
	*/
	var rows = th,s = ((n * p)-n);
	for (i = s; i < (s+n) && i < tr.length; i++)
		rows += tr[i];
	
	// now the table has a processed group of rows ..
	a.innerHTML = rows;
	// create the pagination buttons
	document.getElementById("buttons").innerHTML = pageButtons(pageCount,p);
	// CSS Stuff
	document.getElementById("id"+p).setAttribute("class","active");
}

// ($pCount) : number of pages,($cur) : current page, the selected one ..
function pageButtons(pCount,cur) {
	/* this variables will disable the "Prev" button on 1st page
	   and "next" button on the last one */
	var cur;
	var	prevDis = (cur == 1)?"disabled":"",
	var	nextDis = (cur == pCount)?"disabled":"",
		/* this ($buttons) will hold every single button needed
		** it will creates each button and sets the onclick attribute
		** to the "sort" function with a special ($p) number..
		*/
		var buttons = "<input type='button' value='<< Prev' onclick='sort("+(cur - 1)+")' "+prevDis+">";
	for (var i=1; i<=pCount;i++)
		buttons += "<input type='button' id='id"+i+"'value='"+i+"' onclick='sort("+i+")'>";
	buttons += "<input type='button' value='Next >>' onclick='sort("+(cur + 1)+")' "+nextDis+">";
	return buttons;
}
