import os
import re
import gitlab
#import requests
import sys
import time
from datetime import datetime, date
#from jinja2 import Environment, FileSystemLoader
import csv

#url = str(sys.argv[1])
#token = str(sys.argv[2]) 
#p_id = str(sys.argv[3])

def main():
     print ("START MAIN")

#job_start = os.environ['CI_JOB_STARTED_AT']  # or CI_PIPELINE_CREATED_AT
#page_updated_at = datetime.strptime(job_start,"%Y-%m-%dT%H:%M:%S%z").date()
page_updated_at = date.today()
gl = gitlab.Gitlab(url='https://gitlab.com', private_token='glpat-eDz_tKfBsXda-FoHbW64')
project = gl.projects.get(45588617)
issue = project.issues.list(state='opened', order_by="created_at", sort="asc")
title = []
desc = []
iid = []
issueURL = []
state = []
author = []
assignee = []
confidential = []
locked = []
due_date = []
created_at = []
updated_at = []
closed_at = []
milestone = []
#weight = []
labels = []
time_estimate = []
time_spent = []
#epic_id = []
#epic_title = []
pod = []
product = []
lead = []
vendor = []
vendor_i_type = []
work = []
priority = []

for i in issue:
    title.append(i.title)
    desc.append(i.description)
    iid.append(i.iid)
    issueURL.append(i.web_url)
    ll = i.labels
    dict_kv = {}
    for kv in ll:
        try:
            k = kv.split("::")[0].strip()
            v = kv.split("::")[1].strip()
            dict_kv[k]=v
        except:
            pass
#POD    
    try:
        pod.append(dict_kv["Pod"])
    except:
        pod.append("N/A")
#Product    
    try:
        product.append(dict_kv["Product"])
    except:
        product.append("N/A")
#Lead    
    try:
        lead.append(dict_kv["Lead"])
    except:
        lead.append("N/A")
#Vendor    
    try:
        vendor.append(dict_kv["Vendor"])
    except:
        vendor.append("N/A")
#Vendor Issue Type    
    try:
        vendor_i_type.append(dict_kv["Vendor Issue Type"])
    except:
        vendor_i_type.append("N/A")
#Work    
    try:
        work.append(dict_kv["Work"])
    except:
        work.append("N/A")
#Priority    
    try:
        priority.append(dict_kv["Priority"])
    except:
        priority.append("N/A")
#Assignee    
    try:
        assignee.append(i.assignee['name'])
    except:
        assignee.append("N/A")
#Author    
    try:
        author.append(i.author['name'])
    except:
        author.append("N/A")
#Closed_At   
    try:
        closed_at.append(f'{(datetime.strptime(i.closed_at,"%Y-%m-%dT%H:%M:%S.%f%z").date())}')
    except:
        closed_at.append("N/A")
#Milestone    
    try:
        milestone.append(i.milestone)
    except:
        milestone.append("N/A")

    confidential.append(i.confidential)
    locked.append(i.discussion_locked)
    due_date.append(i.due_date)
    try:
        created_at.append(f'{(datetime.strptime(i.created_at,"%Y-%m-%dT%H:%M:%S.%f%z").date())}')
    except:
        created_at.append("N/A")
    try:
        updated_at.append(f'{(datetime.strptime(i.updated_at,"%Y-%m-%dT%H:%M:%S.%f%z").date())}')
    except:
        updated_at.append("N/A")
    #issue_age.append(f'{(date.today() - (datetime.strptime(i.created_at,"%Y-%m-%dT%H:%M:%S.%f%z").date())).days} days')
   # weight.append(i.weight)
    state.append(i.state)
    labels.append(i.labels)
    time_estimate.append(i.title)
    time_spent.append(i.title)        

num = range(1,len(iid)+1)
headers = ['S.No', 'Title', 'Description', 'Issue Id', 'Issue Url', 'State', 'Author', 'Assignee', 'Confidential', 'Locked', 'Due Date', 'Created At', 'Updated At', 'Closed At', 'Milestone', 'Weight', 'Labels', 'Time Estimate', 'Time Spent', 'POD', 'Product', 'Lead', 'Vendor', 'Vendor Type', 'Work', 'Priority']
data = zip(num, title, desc, iid, issueURL, state, author, assignee, confidential, locked, due_date, created_at, updated_at, closed_at, milestone, labels, time_estimate, time_spent, pod, product, lead, vendor, vendor_i_type, work, priority)
result = list(data)
print(result)
#csvfile = 'C:/Users/SH40080148/Documents/example.csv'

with open("example.csv", 'w', newline='') as output:
    writer = csv.writer(output)
    writer.writerow(headers)
#        ['S.No', 'Title', 'Description', 'Issue Id', 'Issue Url', 'State', 'Author', 'Assignee', 'Confidential', 'Locked', 'Due Date', 'Created At', 'Updated At', 'Closed At', 'Milestone', 'Labels', 'Time Estimate', 'Time Spent', 'POD', 'Product', 'Lead', 'Vendor', 'Vendor Type', 'Work', 'Priority'])
    writer.writerows(result)


if __name__== "__main__":
     main()
