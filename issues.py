import os
import gitlab
import requests
import sys
import time
from tabulate import tabulate

def main():
     print ("START MAIN")
     
gl = gitlab.Gitlab(url='https://gitlab.com', private_token='glpat-eDz_tKfBsXda-FoHbW64')
project = gl.projects.get(45588617)
issue = project.issues.list(labels=['Missing Info'])
iid = []
assignee = []
issueURL = []
lables = []
notes = []

string1 = "Invalid:"

for i in issue:
    if (i.assignee==None):
        assignee.append(i.author['name'])
    else:
        assignee.append(i.assignee['name'])
    iid.append(i.iid)
    issueURL.append(i.web_url)
    lables.append(i.labels)
    note = i.notes.list()
    for n in note:
        if (n.body.count(string1) > 0 and n.noteable_iid==i.iid):
              #  print(n.updated_at)
                notes.append(n.body)
        else:
            " "
    num = range(1,len(iid)+1)
    headers = ['S.No', 'Assignee Name', 'Issue Link', 'labels', 'Comments']
    data = zip(num, assignee, issueURL, lables, notes) 

    print ("Creating Gitlab Pages")

    html = """
<html>
<head>
<title>GitLab Dashboard</title>
<script type="text/javascript">
function myFunction() {{
var input, filter, table, tr, td, i;
input = document.getElementById("myinput");
fitler = input.value.toUpperCase();
table = document.getElementsByTagName("table")[0];
tr = table.getElementsByTagName("tr");
for (i=1;i<tr.length;i++){{
tr[i].style.display = "none";
td = tr[i].getElementsByTagName("td");
for (var j = 0; j < td.length; j++) {{
cell = tr[i].getElementsByTagName("td")[j];
if(cell){{
if (cell.innerHTML.toUpperCase().indexOf(filter) > -1){{
tr[i].style.display="";
}}
}}
}}
}}

function exportToExcel(){{
var downloadurl;
var dataFileType = 'application/vnd.ms-excel';
var tableSelect = document.getElementsByTagName("table")[0];
var tableHTMLData = tableSelect.outerHTML.replace(/ /g, '%20');

filename = 'issues.xls';

downloadurl = document.createElement("a");
document.body.appendChild(downloadurl);

if(navigator.msSaveOrOpenBlob){{
var blob = new Blob(['ufeff',tableHTMLData], {{
type: dataFileType
}});
navigator.msSaveOrOpenBlob(blob, filename);
}}else{{
downloadurl.href = 'data:' dataFileType + ', ' + tableHTMLData;
downloadurl.download = filename;

download.click();
}}
}}
</script>
</head>
<style>
table,th,td {{ border: 1px solid black; border-collapse: collapse;}}
th, td {{ padding: 5px; }}
background-image: url('/css/searchicon.png');
background-repeat: no-repeat;
width: 50%;
font-size: 16px;
padding: 12px 20px 12px 40px;
border: 1px solid #ddd;
margin-bottom: 12px;
}}
tr.header, tr:hover {{
background-color: #f1f1f1;
}}
button {{
cursor: pointer;
margin-top: 1rem;
}}
</style>

<body>
<h4> Search the table for any module :
<input type:"text" id="myinput" onkeyup="myFunction()" placeholder="Search String .." title="Type in a name">
</h4>

<!-- a href="issues.csv" download="issues.csv">Download Your CSV File here</a-->

<button onclick="exportToExcel()">Export Table Data to Excel File</button>
{table}
</body>
</html>
    """
    html=html.format(table=tabulate(data, headers=headers, tablefmt="html"))
    Html_file=open('./public/index.html','w')
    Html_file.write(html)
    Html_file.close()

    print ("DONE")

if __name__== "__main__":
     main()
