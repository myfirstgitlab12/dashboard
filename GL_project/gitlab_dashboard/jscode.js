// Down Excel File

function exportToExcel(){
    var downloadurl;
    var dataFileType = 'application/vnd.ms-excel';
    var tableSelect = document.getElementsByTagName("table")[0];
    var tableHTMLData = tableSelect.outerHTML.replace(/ /g, '%20');
      
    filename = 'issues.xls';
        
    downloadurl = document.createElement("a");
    document.body.appendChild(downloadurl);
        
    if(navigator.msSaveOrOpenBlob){
    var blob = new Blob(['ufeff', tableHTMLData], {
        type: dataFileType
    });
    navigator.msSaveOrOpenBlob(blob, filename);
    }else{
    downloadurl.href =  'data:' + dataFileType + ', ' + tableHTMLData;
    downloadurl.download = filename;
        
    downloadurl.click();
    }
}

// Dropdown Filter
 function getUniqueValuesFromColoum(){
	var unique_col_value = {}
	
		allfilter = document.querySelectorAll(".table-filter")
		allfilter.forEach((filter_i)=>{
			col_index = filter_i.parentElement.getAttribute("col-index");
			const rows = document.querySelectorAll("#tbl > tbody > tr")
			rows.forEach((row)=>{
				cell_value = row.querySelector("td:nth-child("+ col_index +")").innerHTML;
				if(col_index in unique_col_value){
					if (unique_col_value[col_index].includes(cell_value)){
					}else{
						unique_col_value[col_index].push(cell_value)
					}
									
				}else{
					unique_col_value[col_index] = new Array(cell_value)
				}
			});
		});
		updateSelectOptions(unique_col_value)
			
}
// Add <option> tags

function updateSelectOptions(unique_col_value){
	allfilter = document.querySelectorAll(".table-filter")
	allfilter.forEach((filter_i) => {
			col_index = filter_i.parentElement.getAttribute("col-index")
			unique_col_value[col_index].forEach((i) => {
			  filter_i.innerHTML = filter_i.innerHTML + `\n<option value="${i}">${i}</option>` 
			});
	});
}
// create filter row functions

function filter_rows(){
	allfilter = document.querySelectorAll(".table-filter")
	var filter_value = {}
	
	allfilter.forEach((filter_i)=>{
		col_index = filter_i.parentElement.getAttribute("col-index")
		
		value = filter_i.value
		if(value != "all"){
			filter_value[col_index] = value;		
		}

	});
	
	var col_cell_value = {};

	const rows = document.querySelectorAll("#tbl > tbody > tr");
	rows.forEach((row)=>{
		var display_row = true;

		allfilter.forEach((filter_i)=>{
			col_index = filter_i.parentElement.getAttribute("col-index");
			console.log(col_index)
			col_cell_value[col_index] = row.querySelector("td:nth-child("+ col_index + ")").innerHTML;
		})
		for (var col_i in filter_value){
			filter_val = filter_value[col_i]
			row_cell_value = col_cell_value[col_i]
			
			if(row_cell_value.indexOf(filter_val) == -1 && filter_val != "all" ){
				display_row = false;
				break;
			}
		}
		if(display_row == true){
			row.style.display = "table-row"
		}else {
			row.style.display = "none"
		}
	})
}
window.onload = () => {
	//document.querySelector("#tbl > tbody > tr:nth-child(1) > td:nth-child(2)").innerHTML;
  getUniqueValuesFromColoum();
}
