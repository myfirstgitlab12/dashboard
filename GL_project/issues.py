import os
import gitlab
import requests
import sys
import time
from datetime import datetime, date
from jinja2 import Environment, FileSystemLoader

#url = str(sys.argv[1])
#token = str(sys.argv[2]) 
#p_id = str(sys.argv[3])

def main():
     print ("START MAIN")

#job_start = os.environ['CI_JOB_STARTED_AT']  # or CI_PIPELINE_CREATED_AT
#page_updated_at = datetime.strptime(job_start,"%Y-%m-%dT%H:%M:%S%z").date()

gl = gitlab.Gitlab(url='https://gitlab.com', private_token='glpat-eDz_tKfBsXda-FoHbW64')
project = gl.projects.get(45588617)
issue = project.issues.list(labels=['Missing Info'],state='opened')
iid = []
assignee = []
issueURL = []
issue_age = []
notes = []
issue_type = []
string1 = "Invalid:"
Desc_count=0
Assignee_count=0
Epic_count=0
Labels_count=0

for i in issue:
        note = i.notes.list()
        if (i.description == ''):
            for n in note: 
                if ("Missing Description" in n.body):
                    print ("No Comments Required")
                else:
                    i.notes.create({'body':'Invalid Missing Description'})            
            type = n.body
            if "Description" in type:
                if (i.assignee==None):
                    assignee.append(i.author['name'])
                else:
                    assignee.append(i.assignee['name'])
                iid.append(i.iid)
                issueURL.append(i.web_url)
#                    lables.append(i.labels)
                notes.append(n.body)
                issue_age.append(f'{(date.today() - (datetime.strptime(i.created_at,"%Y-%m-%dT%H:%M:%S.%f%z").date())).days} days')
                issue_type.append("Missing Description")
                Desc_count = Desc_count + 1
        else:
             print("")

print(iid)
print(assignee)
print(issueURL)
print(issue_age)
print(notes)
print(issue_type)

print("Description Count = ", Desc_count)
print("Assignee Count = ", Assignee_count)
print("Labels Count = ", Labels_count)
print("Epic Count = ", Epic_count)
total = (Desc_count + Assignee_count + Labels_count + Epic_count)
print(total)
num = range(1,len(iid)+1)
headers = ['S.No', 'Issue Type', 'Assignee Name', 'Issue Link', 'issue_age', 'Comments']
data = zip(num, issue_type, assignee, issueURL, issue_age, notes, ) 

print ("Creating Gitlab Pages")

html_file_path=f"/gitlab_report/gitlab_dashboard1.html"
template_dir=f"GL_project/template/"
jinja_env = Environment(loader=FileSystemLoader(template_dir))
template = jinja_env.get_template('gitlab_dashboard.html')

#html = html.format(table=tabulate(data, headers=headers, tablefmt="unsafehtml"))
with open('GL_project/gitlab_dashboard/index.html','w') as fh:
    fh.write(template.render(
        issue_header=headers,
        issue_data=data,
        current_date=date.today(),
        Desc_count = Desc_count,
        Assignee_count = Assignee_count,
        Epic_count = Epic_count,
        Labels_count = Labels_count,
        sum = total,
        #page_updated_at = page_updated_at

))

print ("DONE")

if __name__== "__main__":
     main()
