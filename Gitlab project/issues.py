import os
import gitlab
import requests
import sys
import time
from tabulate import tabulate
from datetime import datetime, date
from jinja2 import Environment, FileSystemLoader

def main():
     print ("START MAIN")

job_start = os.environ['CI_JOB_STARTED_AT']  # or CI_PIPELINE_CREATED_AT
print(job_start)

gl = gitlab.Gitlab(url='https://gitlab.com', private_token='glpat-eDz_tKfBsXda-FoHbW64')
project = gl.projects.get(45588617)
issue = project.issues.list(labels=['Missing Info'],state='opened')
iid = []
assignee = []
issueURL = []
lables = []
notes = []
issue_type = []
string1 = "Invalid:"
Desc_count=0
Assignee_count=0
Epic_count=0
Labels_count=0

for i in issue:
        note = i.notes.list()
        for n in note: 
            new_date = datetime.strptime(n.updated_at,"%Y-%m-%dT%H:%M:%S.%f%z").date()    
            if (new_date == date.today()):
                type = n.body
                if "Description" in type:
                    if (i.assignee==None):
                        assignee.append(i.author['name'])
                    else:
                        assignee.append(i.assignee['name'])
                    iid.append(i.iid)
                    issueURL.append(i.web_url)
                    lables.append(i.labels)
                    notes.append(n.body)
                    issue_type.append("Missing Description")
                    Desc_count = Desc_count + 1
                elif "Assignee" in type:
                    if (i.assignee==None):
                        assignee.append(i.author['name'])
                    else:
                        assignee.append(i.assignee['name'])
                    iid.append(i.iid)
                    issueURL.append(i.web_url)
                    lables.append(i.labels)
                    notes.append(n.body)
                    issue_type.append("Missing Assignee")
                    Assignee_count = Assignee_count + 1
                elif "Missing Mandate Labels" in type:
                    if (i.assignee==None):
                        assignee.append(i.author['name'])
                    else:
                        assignee.append(i.assignee['name'])                    
                    iid.append(i.iid)
                    issueURL.append(i.web_url)
                    lables.append(i.labels)
                    notes.append(n.body)
                    issue_type.append("Missing Mandate Labels")
                    Labels_count = Labels_count + 1
                elif "Missing Epic" in type:
                    if (i.assignee==None):
                        assignee.append(i.author['name'])
                    else:
                        assignee.append(i.assignee['name'])
                    iid.append(i.iid)
                    issueURL.append(i.web_url)
                    lables.append(i.labels)
                    notes.append(n.body)
                    issue_type.append("Missing Epic") 
                    Epic_count = Epic_count + 1                                             
                else:
                    print("No New Comments")
        else:
             print("")

print(iid)
print(assignee)
print(issueURL)
print(lables)
print(notes)
print(issue_type)
print("Description Count = ", Desc_count)
print("Assignee Count = ", Assignee_count)
print("Labels Count = ", Labels_count)
print("Epic Count = ", Epic_count)
sum = (Desc_count + Assignee_count + Labels_count + Epic_count)
print(sum)
num = range(1,len(iid)+1)
headers = ['S.No', 'Assignee Name', 'Issue Link', 'labels', 'Comments', 'Issue Type']
data = zip(num, assignee, issueURL, lables, notes, issue_type) 

print ("Creating Gitlab Pages")

html_file_path=f"./template/gitlab_dashboard.html"
template_dir=f"template/"
jinja_env = Environment(loader=FileSystemLoader(template_dir))
template = jinja_env.get_template("gitlab_dashboard.html")

#html = html.format(table=tabulate(data, headers=headers, tablefmt="unsafehtml"))
with open('./gitlab_dashboard/gitlab_dashboard1.html','w') as fh:
    fh.write(template.render(
        issue_header=headers,
        issue_data=data,
        current_date=date.today(),
        Desc_count = Desc_count,
        Assignee_count = Assignee_count,
        Epic_count = Epic_count,
        Labels_count = Labels_count,
        sum = sum,
        job_date=job_start

))

print ("DONE")

if __name__== "__main__":
     main()
